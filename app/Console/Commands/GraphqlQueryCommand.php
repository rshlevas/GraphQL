<?php

namespace App\Console\Commands;

use Symfony\Component\Console\Input\InputOption;

class GraphqlQueryCommand extends GraphqlBaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'graphql:query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new GraphQL query class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'GraphQL query';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if ($this->option('paginate')) {
            return __DIR__.'/stubs/graphql.query.paginate.stub';
        }
        return __DIR__.'/stubs/graphql.query.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\GraphQL\Query';
    }

    /**
     * Build the class with the given name.
     *
     * Remove the base controller import if we are already in base namespace.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $replace = [];

        if ($this->option('model')) {
            $replace = $this->buildModelReplacements($replace);
        }

        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', 'm', InputOption::VALUE_OPTIONAL, 'Generate a query with related model'],
            ['paginate', 'p', InputOption::VALUE_NONE, 'Generate a query with pagination'],
        ];
    }
}

