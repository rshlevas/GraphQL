<?php

namespace App\Console\Commands;

use Symfony\Component\Console\Input\InputOption;

class GraphqlTypeCommand extends GraphqlBaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'graphql:type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new GraphQL type class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'GraphQL type';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/graphql.type.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\GraphQL\Type';
    }

    /**
     * Build the class with the given name.
     *
     * Remove the base controller import if we are already in base namespace.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $replace = $this->getOptionalReplacements();


        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    /**
     * Function that checks the optional values and prepare necessary replacements
     *
     * @return array
     */
    protected function getOptionalReplacements() : array
    {
        $replace = [];
        $options = $this->options();
        $optionalModels = array_filter(array_slice($options, 0, 3));
        $functionsMap = $this->getOptionalFunctionsMap();

        foreach ($optionalModels as $option => $model) {
            $replace = $this->{$functionsMap[$option]}($replace);
        }

        return $replace;
    }

    /**
     * Map of optional replacements builders
     *
     * @return array
     */
    private function getOptionalFunctionsMap()
    {
        return [
            'model' => 'buildModelReplacements',
            'relation-to-many' => 'buildToManyRelationsReplacements',
            'relation-to-one' => 'buildToOneRelationsReplacements'
        ];
    }

    /**
     * Builds replacements for many-to-one and one-to-one relationships
     *
     * @param array $replace
     * @return array
     */
    protected function buildToOneRelationsReplacements(array $replace)
    {
        $modelClass = $this->prepareModel($this->option('relation-to-one'));

        return array_merge($replace, [
            'BelongsToOneRelationAttribute' => lcfirst(class_basename($modelClass)),
            'NameOfOneRelatedModelGraphQLType' => lcfirst(class_basename($modelClass)),
            'BelongToOneModel' => class_basename($modelClass),
        ]);
    }

    /**
     * Builds replacements for one-to-many and many-to-many relationships
     *
     * @param array $replace
     * @return array
     */
    protected function buildToManyRelationsReplacements(array $replace)
    {
        $modelClass = $this->prepareModel($this->option('relation-to-many'));

        return array_merge($replace, [
            'BelongsToManyRelationAttribute' => lcfirst(class_basename($modelClass)).'s',
            'BelongToManyModel' => class_basename($modelClass),
            'NameOfManyRelatedModelGraphQLType' => lcfirst(class_basename($modelClass)),
        ]);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', 'm', InputOption::VALUE_OPTIONAL, 'Generate a query with related model'],
            ['relation-to-many', 'r', InputOption::VALUE_OPTIONAL, 'Model with To many relations to this model'],
            ['relation-to-one', 'o', InputOption::VALUE_OPTIONAL, 'Model with To one relations to this model'],
        ];
    }
}

