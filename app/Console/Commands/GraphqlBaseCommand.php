<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use InvalidArgumentException;

abstract class GraphqlBaseCommand extends GeneratorCommand
{
    /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     * @return array
     */
    protected function buildModelReplacements(array $replace)
    {
        $modelClass = $this->prepareModel($this->option('model'));

        return array_merge($replace, [
            'DummyFullModelClass' => $modelClass,
            'DummyModelClass' => class_basename($modelClass),
            'DummyModelVariable' => lcfirst(class_basename($modelClass)),
            'your_type_name' => lcfirst(class_basename($modelClass)),
        ]);
    }

    /**
     * Prepare model name, that user insert for further usage
     *
     * @param string $name
     * @return string
     */
    protected function prepareModel(string $name)
    {
        $modelClassVariants = $this->parseModel($name);
        $modelClass = $this->selectModelClassFromVariants($modelClassVariants);
        $this->checkModelExistance($modelClass);

        return $modelClass;
    }

    /**
     * @param string $modelClass
     */
    protected function checkModelExistance(string $modelClass)
    {
        if (! class_exists($modelClass)) {
            if ($this->confirm("A {$modelClass} model does not exist. Do you want to generate it?", true)) {
                $this->call('make:model', ['name' => class_basename($modelClass)]);
            }
        }
    }

    /**
     * @param array $variants
     * @return string
     */
    protected function selectModelClassFromVariants(array $variants) {
        $model = $this->choice('Where is your model present?', $variants);
        return $model;
    }

    /**
     * Get the fully-qualified model class name variants.
     *
     * @param $model
     * @return array
     */
    protected function parseModel($model)
    {   $modelVariants = [];
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        $model = trim(str_replace('/', '\\', $model), '\\');

        if (! Str::startsWith($model, $rootNamespace = $this->laravel->getNamespace())) {
            $modelVariants[] = $rootNamespace.$model;
            $modelVariants[] = $rootNamespace.'Model\\'.$model;
        }

        return $modelVariants;
    }
}