<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GraphqlMutationCommand extends Command
{
    protected $callParams;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'graphql:generate:mutations 
                            {name : A name of model for which mutations will be created}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commands for creation of graphql mutation files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->callParams = $this->getCallParams();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Generating files...");
        $this->generateFiles($this->argument('name'));
        $this->info("Files were created");
    }

    /**
     * @return array
     */
    protected function getCallParams()
    {
        return [
            'New' => 'graphql:mutation:create',
            'Update' => 'graphql:mutation:update',
            'Delete' => 'graphql:mutation:delete'
        ];
    }

    /**
     * @param string $name
     */
    protected function generateFiles(string $name)
    {
        foreach ($this->callParams as $key => $value) {
            $this->generateFile($value, $name, $key);
        }
    }

    /**
     * @param $command
     * @param $name
     * @param $prefix
     * @return int
     */
    protected function generateFile($command, $name, $prefix)
    {
        $commandOptions = [
            'name' => "{$prefix}{$name}Mutation",
            '--model' => $name,
        ];

        return $this->call($command, $commandOptions);
    }
}

