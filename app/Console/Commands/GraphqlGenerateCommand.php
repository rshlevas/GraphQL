<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GraphqlGenerateCommand extends Command
{
    protected $query;

    protected $type;

    private $queryOptions;

    private $typeOptions;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'graphql:generate 
                            {name : A name query and type}
                            {--p|paginate}
                            {--m|model= : Model}
                            {--o|relation-to-one= : To one model relation}
                            {--r|relation-to-many= : To many model relation}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commands for creation of graphql type and query files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setFilesName($this->argument('name'));
        $this->setCommandsOptions($this->getOptions());
        $this->info("Creating {$this->query}...");
        $this->createQueryFile();
        $this->info("Creating {$this->type}...");
        $this->createTypeFile();
        $this->info("Files were created");
    }

    /**
     * @return int
     */
    protected function createTypeFile()
    {
        $commandOptions = ['name' => $this->type];
        $commandOptions = array_merge($commandOptions, $this->typeOptions);

        return $this->call('graphql:type', $commandOptions);
    }

    /**
     * @return int
     */
    protected function createQueryFile()
    {
        $commandOptions = ['name' => $this->query];
        $commandOptions = array_merge($commandOptions, $this->queryOptions);

        return $this->call('graphql:query', $commandOptions);
    }

    /**
     * @return 0|array
     */
    protected function getOptions()
    {
        return array_slice($this->options(), 0, 4);
    }

    /**
     * Returns query file name
     *
     * @return string
     */
    protected function getQueryName($name)
    {
        return $name . 'Query';
    }

    /**
     * Returns type file name
     *
     * @return string
     */
    protected function getTypeName($name)
    {
        return $name . 'Type';
    }

    /**
     * @param $name
     */
    protected function setFilesName($name)
    {
        $this->setQueryName($name)->setTypeName($name);
    }

    /**
     * @param $name
     * @return $this
     */
    protected function setQueryName($name)
    {
        $this->query = $this->getQueryName($name);

        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    protected function setTypeName($name)
    {
        $this->type = $this->getTypeName($name);

        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function setCommandsOptions(array $params)
    {
        return $this->setTypeOptions($params)->setQueryOptions($params);
    }

    /**
     * @param $params
     * @param $offset
     * @param $length
     * @return array
     */
    protected function getTypeOptionsFromParams($params, $offset, $length)
    {
        $typeOptions = [];
        $params = array_slice($params, $offset, $length);
        $options = array_keys($params);
        foreach ($options as $option) {
            $typeOptions['--'.$option] = $params[$option];
        }

        return $typeOptions;
    }

    /**
     * @param $params
     * @return $this
     */
    protected function setTypeOptions($params)
    {
        $this->typeOptions = $this->getTypeOptionsFromParams($params, 1, 3);

        return $this;
    }

    /**
     * @param $params
     * @return $this
     */
    protected function setQueryOptions($params)
    {
        $this->queryOptions = $this->getTypeOptionsFromParams($params, 0, 2);

        return $this;
    }
}
