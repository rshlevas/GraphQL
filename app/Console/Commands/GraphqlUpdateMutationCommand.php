<?php

namespace App\Console\Commands;

use Symfony\Component\Console\Input\InputOption;

class GraphqlUpdateMutationCommand extends GraphqlBaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'graphql:mutation:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new GraphQL update mutation class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'GraphQL updateMutation';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/graphql.update.mutation.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\GraphQL\Mutation';
    }

    /**
     * Build the class with the given name.
     *
     * Remove the base controller import if we are already in base namespace.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $replace = [];

        if ($this->option('model')) {
            $replace = $this->buildModelReplacements($replace);
        }

        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', 'm', InputOption::VALUE_REQUIRED, 'Generate a update mutation with related model'],
        ];
    }
}

