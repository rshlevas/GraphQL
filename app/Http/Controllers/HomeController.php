<?php

namespace App\Http\Controllers;

use App\Category;
use App\Model\Product;
use App\Model\ProductWarehouse;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = ProductWarehouse::find(1);
        dd($product->product);
        $user = Auth::user();
        $name = $user->user_profile->first_name;
        $surname = UserProfile::whereLastName($name)->get();
        dd($surname);

        $categories = Category::find(1);

        dd($categories->products());

        return view('home');
    }
}
