<?php

namespace App\GraphQL\Type\Scalar;

class ScalarType
{
    const DATE = 'Date';

    /**
     * @var
     */
    private static $internalTypes;

    /**
     * @return array|mixed
     */
    public static function date()
    {
        return self::getInternalType(self::DATE);
    }

    /**
     * @param null $name
     * @return array|mixed
     */
    private static function getInternalType($name = null)
    {
        if (null === self::$internalTypes) {
            self::$internalTypes = [
                self::DATE => new DateType()
            ];
        }
        return $name ? self::$internalTypes[$name] : self::$internalTypes;
    }
}