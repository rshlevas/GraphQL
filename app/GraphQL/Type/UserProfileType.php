<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;
use App\Model\UserProfile;

class UserProfileType extends GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'user_profile',
        'description' => 'Profile of the user',
        'model' => UserProfile::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the user_profile',
            ],
            'user_id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'User id related to this profile',
            ],
            'first_name' => [
                'type' => Type::string(),
                'description' => 'User first name'
            ],
            'last_name' => [
                'type' => Type::string(),
                'description' => 'User last name',
            ],
            'phone_number' => [
                'type' => Type::string(),
                'description' => 'User phone number'
            ],
            'user' => [
                'type' => GraphQL::type('user'),
                'description' => 'User model related to this profile'
            ],
        ];
    }
}