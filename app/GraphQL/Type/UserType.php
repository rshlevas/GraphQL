<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;
use App\User;

class UserType extends GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'user',
        'description' => 'User model type',
        'model' => User::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the user',
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'Email of the user',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'Username',
            ],
            'user_profile' => [
                'type' => GraphQL::type('user_profile'),
                'description' => 'Profile of the user'
            ],
            'comments' => [
                'type' => Type::listOf(GraphQL::type('comment')),
                'description' => 'User comments'
            ],
            'orders' => [
                'type' => Type::listOf(GraphQL::type('order')),
                'description' => 'User orders'
            ],
            'favoriteProducts' => [
                'type' => Type::listOf(GraphQL::type('products')),
                'description' => 'Products, which user want to buy'
            ],
            'viewedProducts' => [
                'type' => Type::listOf(GraphQL::type('products')),
                'description' => 'Products, viewed by user'
            ]
        ];
    }
}