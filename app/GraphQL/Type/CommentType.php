<?php

namespace App\GraphQL\Type;

use App\GraphQL\Type\Scalar\ScalarType;
use App\Model\Comment;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;

class CommentType extends GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'comment',
        'description' => 'Comment of the products',
        'model' => Comment::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the user_profile',
            ],
            'user_id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'User id related to this profile',
            ],
            'content' => [
                'type' => Type::string(),
                'description' => 'Comment content'
            ],
            'created_at' => [
                'type' => ScalarType::date(),
                'description' => 'Date of the comment',
            ],
            'user' => [
                'type' => GraphQL::type('user'),
                'description' => 'Comment author'
            ],
            'product' => [
                'type' => GraphQL::type('products'),
                'description' => 'Comment target product'
            ]
        ];
    }
}