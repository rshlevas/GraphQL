<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;
use App\Model\Category;

class CategoryType extends  GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'category',
        'description' => 'Category model type',
        'model' => Category::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the category',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of category',
            ],
            'products' => [
                'type' => Type::listOf(GraphQL::type('products')),
                'description' => 'Products related to category'
            ]
        ];
    }
}