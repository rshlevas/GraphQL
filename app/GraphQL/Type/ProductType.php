<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;
use App\Model\Product;

class ProductType extends GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'products',
        'description' => 'Product model type',
        'model' => Product::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the product',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of product',
            ],
            'price' => [
                'type' => Type::string(),
                'description' => 'Price of the product'
            ],
            'short_description' => [
                'type' => Type::string(),
                'description' => 'Short description of the product'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'Product description'
            ],
            'category_id' => [
                'type' => Type::int(),
                'description' => 'Category id'
            ],
            'category' => [
                'type' => GraphQL::type('category'),
                'description' => "related category of the product"
            ],
            'producer_id' => [
                'type' => Type::int(),
                'description' => 'Producer id'
            ],
            'producer' => [
                'type' => GraphQL::type('producer'),
                'description' => "producer of the product"
            ],
            'comments' => [
                'type' => Type::listOf(GraphQL::type('comment')),
                'description' => 'Comments of the product'
            ],
            'warehouseProducts' => [
                'type' => Type::listOf(GraphQL::type('product_warehouse')),
                'description' => 'Warehouse product instances'
            ],
            'subscribers' => [
                'type' => Type::listOf(GraphQL::type('user')),
                'description' => 'Users, that are wanted this product'
            ],
            'viewers' => [
                'type' => Type::listOf(GraphQL::type('user')),
                'description' => 'Users, that viewed product pages'
            ],
            'orderProducts' => [
                'type' => Type::listOf(GraphQL::type('product_order')),
                'description' => 'Order product instances'
            ]
        ];
    }

}