<?php

namespace App\GraphQL\Type;

use App\Model\Warehouse;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;

class WarehouseType extends  GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'warehouse',
        'description' => 'Warehouse model type',
        'model' => Warehouse::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the warehouse',
            ],
            'city' => [
                'type' => Type::string(),
                'description' => 'City of the warehouse',
            ],
            'address' => [
                'type' => Type::string(),
                'description' => 'Address of the warehouse'
            ],
            'products' => [
                'type' => Type::listOf(GraphQL::type('product_warehouse')),
                'description' => 'Products at the warehouse'
            ]
        ];
    }
}