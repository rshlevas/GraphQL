<?php

namespace App\GraphQL\Type;

use App\Model\ProductWarehouse;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;

class ProductWarehouseType extends GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'product_warehouse',
        'description' => 'Product warehouse model type',
        'model' => ProductWarehouse::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the products in warehouse',
            ],
            'count' => [
                'type' => Type::int(),
                'description' => 'Count of products in warehouse',
            ],
            'product_id' => [
                'type' => Type::int(),
                'description' => 'Id of base product'
            ],
            'warehouse_id' => [
                'type' => Type::int(),
                'description' => 'Id of warehouse'
            ],
            'product' => [
                'type' => GraphQL::type('products'),
                'description' => 'Base product'
            ],
            'warehouse' => [
                'type' => GraphQL::type('warehouse'),
                'description' => 'Warehouse'
            ]
        ];
    }
}