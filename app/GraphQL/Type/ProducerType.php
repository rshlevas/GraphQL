<?php

namespace App\GraphQL\Type;

use App\Model\Producer;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;

class ProducerType extends  GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'producer',
        'description' => 'Producer model type',
        'model' => Producer::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the producer',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of producer',
            ],
            'country' => [
                'type' => Type::string(),
                'description' => 'Producer country'
            ],
            'products' => [
                'type' => Type::listOf(GraphQL::type('products')),
                'description' => 'Products of this producer'
            ]
        ];
    }
}