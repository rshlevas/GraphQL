<?php

namespace App\GraphQL\Type;

use App\GraphQL\Type\Scalar\ScalarType;
use App\Model\Order;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;

class OrderType extends GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'order',
        'description' => 'Order entity',
        'model' => Order::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the order',
            ],
            'status' => [
                'type' => Type::int(),
                'description' => 'Order status'
            ],
            'user_id' => [
                'type' => Type::int(),
                'description' => 'Id of order owner'
            ],
            'products' => [
                'type' => Type::listOf(GraphQL::type('product_order')),
                'description' => 'Base product'
            ],
            'user' => [
                'type' => GraphQL::type('user'),
                'description' => 'Owner of the order',
            ],
            'summary' => [
                'type' => Type::string(),
                'description' => 'Whole order price'
            ],
            'created_at' => [
                'type' => ScalarType::date(),
                'description' => 'Date of order creation'
            ],
            'updated_at' => [
                'type' => ScalarType::date(),
                'description' => 'Date of order status update'
            ]
        ];
    }
}