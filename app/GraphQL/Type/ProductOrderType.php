<?php

namespace App\GraphQL\Type;

use App\Model\OrderProduct;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphType;

class ProductOrderType extends GraphType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'product_order',
        'description' => 'Product of order',
        'model' => OrderProduct::class,
    ];

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the products in order',
            ],
            'count' => [
                'type' => Type::int(),
                'description' => 'Count of the products'
            ],
            'product_id' => [
                'type' => Type::int(),
                'description' => 'Id of base product'
            ],
            'order_id' => [
                'type' => Type::int(),
                'description' => 'Id of order'
            ],
            'product' => [
                'type' => GraphQL::type('products'),
                'description' => 'Base product'
            ],
            'order' => [
                'type' => GraphQL::type('order'),
                'description' => 'Order',
            ],
            'item_price' => [
                'type' => Type::string(),
                'description' => 'Price per product Item'
            ],
            'summary' => [
                'type' => Type::string(),
                'description' => 'Summary product price'
            ]
        ];
    }
}