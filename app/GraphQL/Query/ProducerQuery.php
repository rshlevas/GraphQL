<?php

namespace App\GraphQL\Query;

use App\Model\Producer;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProducerQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'Producer',
        'description' => 'A query of producers'
    ];

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ],
            'country' => [
                'name' => 'country',
                'type' => Type::string()
            ]
        ];
    }

    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('producer'));
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return mixed
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $producer = Producer::with($fields->getRelations())
            ->where($where)
            ->get();

        return $producer;
    }
}