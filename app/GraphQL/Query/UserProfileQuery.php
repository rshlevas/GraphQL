<?php

namespace App\GraphQL\Query;

use App\Model\UserProfile;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Auth;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserProfileQuery extends Query
{
    private $auth;

    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'UserProfile',
        'description' => 'A query of user profile'
    ];

    public function authorize(array $args)
    {
        try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
        }
        return (boolean) $this->auth;
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'phone_number' => [
                'name' => 'phone_number',
                'type' => Type::string()
            ],
            'first_name' => [
                'name' => 'first_name',
                'type' => Type::string()
            ],
            'last_name' => [
                'name' => 'last_name',
                'type' => Type::string()
            ],
            'user_id' => [
                'name' => 'user_id',
                'type' => Type::int()
            ],
        ];
    }

    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('user_profile'));
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return mixed
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $userProfile = UserProfile::with($fields->getRelations())
            ->where($where)
            ->get();

        return $userProfile;
    }
}