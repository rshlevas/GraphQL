<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08.12.17
 * Time: 10:25
 */

namespace App\GraphQL\Query;

use App\Model\Order;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class OrderQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'order',
        'description' => 'A query of orders'
    ];

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'user_id' => [
                'name' => 'user_id',
                'type' => Type::int()
            ],
            'status' => [
                'name' => 'status',
                'type' => Type::int()
            ]
        ];
    }

    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('order'));
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return mixed
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $user = Order::with($fields->getRelations())
            ->where($where)
            ->get();

        return $user;
    }
}