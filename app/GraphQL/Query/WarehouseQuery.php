<?php

namespace App\GraphQL\Query;

use App\Model\Warehouse;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class WarehouseQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'Warehouse',
        'description' => 'A query of warehouses'
    ];

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
        ];
    }

    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('warehouse'));
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return mixed
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $warehouse = Warehouse::with($fields->getRelations())
            ->where($where)
            ->get();

        return $warehouse;
    }
}