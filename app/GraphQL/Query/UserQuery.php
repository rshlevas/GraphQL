<?php

namespace App\GraphQL\Query;

use App\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'User',
        'description' => 'A query of users'
    ];

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'email' => [
                'name' => 'email',
                'type' => Type::string()
            ]
        ];
    }

    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return mixed
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $user = User::with($fields->getRelations())
            ->where($where)
            ->get();

        return $user;
    }
}