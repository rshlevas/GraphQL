<?php

namespace App\GraphQL\Query;

use App\Model\Category;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CategoryQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'Category',
        'description' => 'A query of category'
    ];

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ]
        ];
    }

    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('category'));
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $category = Category::with($fields->getRelations())
            ->where($where)
            ->get();

        return $category;
    }
}