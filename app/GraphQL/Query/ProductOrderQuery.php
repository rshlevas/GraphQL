<?php

namespace App\GraphQL\Query;

use App\Model\OrderProduct;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProductOrderQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'product_order',
        'description' => 'A query of order products'
    ];

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'product_id' => [
                'name' => 'product_id',
                'type' => Type::int()
            ],
            'order_id' => [
                'name' => 'order_id',
                'type' => Type::int()
            ]
        ];
    }

    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('product_order'));
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return mixed
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $productsWarehouse = OrderProduct::with($fields->getRelations())
            ->where($where)
            ->get();

        return $productsWarehouse;
    }
}