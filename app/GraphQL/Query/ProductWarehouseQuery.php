<?php

namespace App\GraphQL\Query;

use App\Model\ProductWarehouse;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProductWarehouseQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'product_warehouse',
        'description' => 'A query of warehouses products'
    ];

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'product_id' => [
                'name' => 'product_id',
                'type' => Type::int()
            ],
            'warehouse_id' => [
                'name' => 'warehouse_id',
                'type' => Type::int()
            ]
        ];
    }

    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('product_warehouse'));
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return mixed
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $productsWarehouse = ProductWarehouse::with($fields->getRelations())
            ->where($where)
            ->get();

        return $productsWarehouse;
    }
}