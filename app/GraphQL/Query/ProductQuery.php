<?php

namespace App\GraphQL\Query;

use App\Model\Product;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;
use App\GraphQL\Traits\Paginable;

class ProductQuery extends Query
{
    use Paginable;

    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'Product Query',
        'description' => 'A query of products'
    ];

    /**
     * @return mixed
     */
    public function type()
    {
        return GraphQL::paginate('products');
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ],
            'price' => [
                'name' => 'price',
                'type' => Type::string()
            ],
            'category_id' => [
                'name' => 'category_id',
                'type' => Type::int()
            ],
            'page' => [
                'name' => 'page',
                'type' => Type::int()
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int()
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $sortedArgs = $this->sortArguments($args);
        $whereArgs = $sortedArgs['where'];
        $pagination = $sortedArgs['pagination'];

        $where = function ($query) use ($whereArgs) {
            foreach ($whereArgs as $key => $value) {
                $query->where($key, $value);
            }
        };

        $products = Product::with($fields->getRelations())
            ->where($where)
            ->select($fields->getSelect())
            ->paginate($pagination['limit'], ['*'], 'page', $pagination['page']);

        return $products;
    }
}