<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08.12.17
 * Time: 10:24
 */

namespace App\GraphQL\Query;

use App\Model\Comment;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CommentQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'Comment',
        'description' => 'A query of comments'
    ];

    /**
     * @return mixed
     */
    public function type()
    {
        return GraphQL::paginate('comment');
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'product_id' => [
                'name' => 'product_id',
                'type' => Type::int()
            ],
            'user_id' => [
                'name' => 'user_id',
                'type' => Type::int()
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string()
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        };

        $comments = Comment::with($fields->getRelations())
            ->where($where)
            ->orderBy('created_at', 'DESC')
            ->select($fields->getSelect())
            ->paginate();

        return $comments;
    }
}