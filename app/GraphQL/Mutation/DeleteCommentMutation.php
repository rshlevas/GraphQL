<?php

namespace App\GraphQL\Mutation;

use App\Model\Comment;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class DeleteCommentMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'DeleteComment'
    ];

    /**
     * @return mixed
     */
    public function type()
    {
        return Type::string();
    }

    /**
     * @param array $args
     * @return bool
     */
    public function authorize(array $args)
    {
        try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
        }
        return (boolean) $this->auth;
    }

    /**
     * Argument list
     *
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int())
            ],
        ];
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'integer'],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return null
     */
    public function resolve($root, $args)
    {
        $comment = Comment::find($args['id']);

        return $comment->delete() ? "Comment was deleted" : "Cannot delete comment with id: {$arg['id']}";

    }
}