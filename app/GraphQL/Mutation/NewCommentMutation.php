<?php

namespace App\GraphQL\Mutation;

use App\Model\Comment;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Auth;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class NewCommentMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'NewComment'
    ];

    /**
     * @return mixed
     */
    public function type()
    {
        return GraphQL::type('comment');
    }

    public function authorize(array $args)
    {
        try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
        }
        return (boolean) $this->auth;
    }

    /**
     * Argument list
     *
     * @return array
     */
    public function args()
    {
        return [
            'content' => [
                'name' => 'content',
                'type' => Type::nonNull(Type::string())
            ],
            'product_id' => [
                'name' => 'product_id',
                'type' => Type::nonNull(Type::int())
            ],
        ];
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => ['required'],
            'product_id' => ['required', 'integer'],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return null
     */
    public function resolve($root, $args)
    {
        $user = Auth::getUser();
        $args['user_id'] = $user->id;

        $comment = Comment::create($args);

        return $comment ? $comment : null;
    }
}