<?php

namespace App\GraphQL\Mutation;

use App\Model\UserProfile;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class UpdateUserProfileMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'updateUserProfile'
    ];

    /**
     * You should enter your UserProfile type name,
     * that you registered at graphql.php config file
     *
     * @return mixed
     */
    public function type()
    {
        return GraphQL::type('user_profile');
    }

    /**
     * Argument list. You have to mention all attributes, that you want to update
     * and UserProfile entity id
     *
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int())
            ],
            'phone_number' => [
                'name' => 'phone_number',
                'type' => Type::string()
            ],
            'first_name' => [
                'name' => 'first_name',
                'type' => Type::string()
            ],
            'last_name' => [
                'name' => 'last_name',
                'type' => Type::string()
            ],
        ];
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attribute' => ['required', 'other rules'],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return null
     */
    public function resolve($root, $args)
    {
        $userProfile = UserProfile::find($args['id']);
        unset($args['id']);
        $userProfile->update($args);

        return $userProfile ? $userProfile : null;
    }
}
