<?php

namespace App\GraphQL\Mutation;

use App\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class CreateUserMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'NewUser'
    ];

    /**
     * You should enter your User type name,
     * that you registered at graphql.php config file
     *
     * @return mixed
     */
    public function type()
    {
        return GraphQL::type('user');
    }

    /**
     * Argument list. You have to mention all attributes, that the user
     * model have
     *
     * @return array
     */
    public function args()
    {
        return [
            'email' => [
                'name' => 'email',
                'type' => Type::nonNull(Type::string())
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', 'unique:users'],
            'name' => ['required', 'alpha_dash'],
            'password' => ['required', 'min:6'],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return null
     */
    public function resolve($root, $args)
    {
        $args['password'] = bcrypt($args['password']);
        $user = User::create($args);
        if ( ! $user) {
            return null;
        }
        $user->user_profile()->create();

        return $user ? $user : null;
    }
}
