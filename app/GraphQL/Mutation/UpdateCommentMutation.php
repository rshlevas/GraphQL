<?php

namespace App\GraphQL\Mutation;

use App\Model\Comment;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class UpdateCommentMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'UpdateComment'
    ];

    /**
     * @return mixed
     */
    public function type()
    {
        return GraphQL::type('comment');
    }

    /**
     * Argument list
     *
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int())
            ],
            'content' => [
                'name' => 'content',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => ['required'],
            'id' => ['required', 'integer'],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return null
     */
    public function resolve($root, $args)
    {
        $comment = Comment::find($args['id']);
        unset($args['id']);
        $comment->update($args);

        return $comment ? $comment : null;
    }
}