<?php

namespace App\GraphQL\Traits;

trait Paginable
{
    /**
     * Separates arguments for pagination and where statement
     *
     * @param array $args
     * @return array
     */
    protected function sortArguments(array $args)
    {
        $sortedArgs['pagination'] = $this->getPaginationArguments($args);
        unset($args['page']);
        unset($args['limit']);
        $sortedArgs['where'] = $args;

        return $sortedArgs;
    }

    /**
     * Select pagination params from input arguments
     *
     * @param array $args
     * @return array
     */
    protected function getPaginationArguments(array $args)
    {
        $pagination = [
            'limit' => null,
            'page' => null,
        ];

        foreach ($pagination as $key => $value) {
            $pagination[$key] = $this->getPaginationArgument($key, $args);
        }

        return $pagination;
    }

    /**
     * Select pagination param by its name
     *
     * @param $key
     * @param $args
     * @return null
     */
    protected function getPaginationArgument($key, $args)
    {
        return isset($args[$key]) ? $args[$key] : null;
    }
}