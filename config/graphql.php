<?php

use App\GraphQL\Query\ProductQuery;
use App\GraphQL\Type\ProductType;
use App\GraphQL\Query\CategoryQuery;
use App\GraphQL\Type\CategoryType;
use App\GraphQL\Type\ProducerType;
use App\GraphQL\Query\ProducerQuery;
use App\GraphQL\Query\UserQuery;
use App\GraphQL\Type\UserType;
use App\GraphQL\Query\UserProfileQuery;
use App\GraphQL\Type\UserProfileType;
use App\GraphQL\Type\CommentType;
use App\GraphQL\Query\CommentQuery;
use App\GraphQL\Type\WarehouseType;
use App\GraphQL\Query\WarehouseQuery;
use App\GraphQL\Type\ProductWarehouseType;
use App\GraphQL\Query\ProductWarehouseQuery;
use App\GraphQL\Query\OrderQuery;
use App\GraphQL\Type\OrderType;
use App\GraphQL\Type\ProductOrderType;
use App\GraphQL\Query\ProductOrderQuery;
use App\GraphQL\Mutation\NewCommentMutation;
use App\GraphQL\Mutation\UpdateCommentMutation;
use App\GraphQL\Mutation\DeleteCommentMutation;
use App\GraphQL\Mutation\UpdateUserProfileMutation;
use App\GraphQL\Mutation\CreateUserMutation;

return [

    // The prefix for routes
    'prefix' => 'graphql',

    // The routes to make GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Route
    //
    // Example:
    //
    // Same route for both query and mutation
    //
    // 'routes' => 'path/to/query/{graphql_schema?}',
    //
    // or define each route
    //
    // 'routes' => [
    //     'query' => 'query/{graphql_schema?}',
    //     'mutation' => 'mutation/{graphql_schema?}',
    // ]
    //
    'routes' => '{graphql_schema?}',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\Rebing\GraphQL\GraphQLController@query',
    //     'mutation' => '\Rebing\GraphQL\GraphQLController@mutation'
    // ]
    //
    'controllers' => \Rebing\GraphQL\GraphQLController::class . '@query',

    // Any middleware for the graphql route group
    'middleware' => [],

    // The name of the default schema used when no argument is provided
    // to GraphQL::schema() or when the route is used without the graphql_schema
    // parameter.
    'default_schema' => 'default',

    // The schemas for query and/or mutation. It expects an array of schemas to provide
    // both the 'query' fields and the 'mutation' fields.
    //
    // You can also provide a middleware that will only apply to the given schema
    //
    // Example:
    //
    //  'schema' => 'default',
    //
    //  'schemas' => [
    //      'default' => [
    //          'query' => [
    //              'users' => 'App\GraphQL\Query\UsersQuery'
    //          ],
    //          'mutation' => [
    //
    //          ]
    //      ],
    //      'user' => [
    //          'query' => [
    //              'profile' => 'App\GraphQL\Query\ProfileQuery'
    //          ],
    //          'mutation' => [
    //
    //          ],
    //          'middleware' => ['auth'],
    //      ]
    //  ]
    //
    'schemas' => [
        'default' => [
            'query' => [
                'products' => ProductQuery::class,
                'category' => CategoryQuery::class,
                'producer' => ProducerQuery::class,
                'user' => UserQuery::class,
                'user_profile' => UserProfileQuery::class,
                'comment' => CommentQuery::class,
                'warehouse' => WarehouseQuery::class,
                'product_warehouse' => ProductWarehouseQuery::class,
                'product_order' => ProductOrderQuery::class,
                'order' => OrderQuery::class,
            ],
            'mutation' => [
                'newComment' => NewCommentMutation::class,
                'updateComment' => UpdateCommentMutation::class,
                'deleteComment' => DeleteCommentMutation::class,
                'updateUserProfile' => UpdateUserProfileMutation::class,
                'newUser' => CreateUserMutation::class,
            ],
            'middleware' => []
        ],
    ],
    
    // The types available in the application. You can then access it from the
    // facade like this: GraphQL::type('user')
    //
    // Example:
    //
    // 'types' => [
    //     'user' => 'App\GraphQL\Type\UserType'
    // ]
    //
    'types' => [
        'products' => ProductType::class,
        'category' => CategoryType::class,
        'producer' => ProducerType::class,
        'user' => UserType::class,
        'user_profile' => UserProfileType::class,
        'comment' => CommentType::class,
        'warehouse' => WarehouseType::class,
        'product_warehouse' => ProductWarehouseType::class,
        'product_order' => ProductOrderType::class,
        'order' => OrderType::class
    ],
    
    // This callable will be passed the Error object for each errors GraphQL catch.
    // The method should return an array representing the error.
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    'error_formatter' => ['\Rebing\GraphQL\GraphQL', 'formatError'],

    // You can set the key, which will be used to retrieve the dynamic variables
    'params_key'    => 'variables',
    
];
