<?php

use Faker\Generator as Faker;
use App\Model\Comment;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 5),
        'content' => $faker->sentence,
        'product_id' => $faker->numberBetween(1, 15),
    ];
});
