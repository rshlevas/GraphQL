<?php

use Faker\Generator as Faker;
use App\Model\ProductWarehouse;

$factory->define(ProductWarehouse::class, function (Faker $faker) {
    return [
        'product_id' => 1,
        'warehouse_id' => 1,
        'count' => $faker->numberBetween(10, 1000),
    ];
});
