<?php

use Faker\Generator as Faker;
use App\Model\UserProfile;;

$factory->define(App\Model\UserProfile::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_number' => $faker->phoneNumber,
        'user_id' => 1
    ];
});
