<?php

use Faker\Generator as Faker;
use App\Model\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});
