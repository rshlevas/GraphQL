<?php

use Faker\Generator as Faker;
use App\Model\Order;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 5),
        'summary' => $faker->numberBetween(10, 1000),
        'status' => 1,
    ];
});
