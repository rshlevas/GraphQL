<?php

use Faker\Generator as Faker;
use App\Model\Warehouse;

$factory->define(Warehouse::class, function (Faker $faker) {
    return [
        'city' => $faker->city,
        'address' => $faker->address,
    ];
});
