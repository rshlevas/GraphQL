<?php

use Faker\Generator as Faker;
use App\Model\Producer;

$factory->define(Producer::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'country' => $faker->country
    ];
});
