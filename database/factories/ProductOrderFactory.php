<?php

use Faker\Generator as Faker;
use App\Model\OrderProduct;

$factory->define(OrderProduct::class, function (Faker $faker) {
    return [
        'product_id' => 1,
        'order_id' => 1,
        'count' => 2,
        'item_price' => 10,
        'summary' => 20,
    ];
});
