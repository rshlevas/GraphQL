<?php

use Faker\Generator as Faker;
use App\Model\Product;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'short_description' => $faker->sentence,
        'description' => $faker->paragraph,
        'price' => $faker->numberBetween(0, 100),
        'category_id' => 1,
        'producer_id' => 1,
    ];
});
