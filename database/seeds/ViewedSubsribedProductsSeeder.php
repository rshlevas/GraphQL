<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ViewedSubsribedProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 5; $i++) {
            DB::table('favorite_products')->insert([
                'product_id' => random_int(1, 10),
                'user_id' => random_int(1, 5)
            ]);

            DB::table('viewed_products')->insert([
                'product_id' => random_int(1, 10),
                'user_id' => random_int(1, 5)
            ]);
        }

    }
}
