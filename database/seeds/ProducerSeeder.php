<?php

use Illuminate\Database\Seeder;
use App\Model\Producer;

class ProducerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Producer::class, 5)->create();
    }
}
