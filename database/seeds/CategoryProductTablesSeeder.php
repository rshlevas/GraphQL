<?php

use Illuminate\Database\Seeder;
use App\Model\Category;
use App\Model\Product;
use App\Model\Producer;

class CategoryProductTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 5)->create()->each(function ($c) {
           for ($i = 0; $i < 3; $i++) {
               $c->products()->save(factory(Product::class)->create([
                   'category_id' => $c->id,
                   'producer_id' => $c->id
               ])   );
           }
        });
    }
}
