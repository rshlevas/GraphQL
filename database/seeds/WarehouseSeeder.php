<?php

use Illuminate\Database\Seeder;
use App\Model\Warehouse;
use App\Model\ProductWarehouse;

class WarehouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Warehouse::class, 3)->create()->each(function ($w) {
            for ($i = 0; $i < 3; $i++) {
                $w->products()->save(factory(ProductWarehouse::class)->create([
                    'product_id' => $i + 1,
                    'warehouse_id' => $w->id,
                ]));
            }
        });
    }
}
