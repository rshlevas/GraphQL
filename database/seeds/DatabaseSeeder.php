<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserTableSeeder::class,
            ProducerSeeder::class,
            CategoryProductTablesSeeder::class,
            CommentSeeder::class,
            OrderSeeder::class,
            WarehouseSeeder::class,
            ViewedSubsribedProductsSeeder::class
        ]);
    }
}
