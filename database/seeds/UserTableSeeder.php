<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Model\UserProfile;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 5)->create()->each(function ($u) {
            $u->user_profile()->save(factory(UserProfile::class)->create(['user_id' => $u->id]));
        });
    }
}
