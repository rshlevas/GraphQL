<?php

use Illuminate\Database\Seeder;
use App\Model\Order;
use App\Model\OrderProduct;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Order::class, 3)->create()->each(function ($o) {
           for ($i = 0; $i < 3; $i++) {
               $o->products()->save(factory(OrderProduct::class)->create([
                   'product_id' => $i + 1,
                   'order_id' => $o->id,
               ]));
           }
        });
    }
}
